# guidebox-php

Guidebox.com PHP Client is a simple but flexible wrapper for the [Guidebox.com](https://api.guidebox.com) API. See full Guidebox.com documentation [here](https://api.guidebox.com/docs).

## Table of Contents

- [Getting Started](#getting-started)
  - [Registration](#registration)
  - [Installation](#installation)
  - [Usage](#usage)
- [Examples](#examples)
- [API Documentation](#api-documentation)
- [Testing](#testing)

## Getting Started

### Registration

First, you will need to first create an account at [Guidebox.com](https://api.guidebox.com/register) and obtain your API Key.

Once you have created an account, you can access your API Key from the [Your API Key](https://api.guidebox.com/docs/key) page.

### Installation

The recommended way to install Guidebox PHP Client is through [Composer](http://getcomposer.org).

Install Composer
```bash
curl -sS https://getcomposer.org/installer | php
```

Add Guidebox.com PHP client as a dependency
```bash
composer require guidebox/guidebox-php
```

After installing, you need to require Composer's autoloader:

```php
require 'vendor/autoload.php';
```

### Usage

Code samples for all endpoints are provided at our [API Documentation](https://api.guidebox.com/docs). Below is a primer on how to get started.

```php
<?php

// Provide an API Key in the class constructor
// in order to instantiate the Guidebox object
$guidebox = new \Guidebox\Guidebox('YOUR_API_KEY');
```

Or set it manually:
```php
<?php

$guidebox = new \Guidebox\Guidebox();
$guidebox->setApiKey('YOUR_API_KEY');
```

## Examples

```
<?php

require __DIR__ . 'vendor/autoload.php';

$client = new Guidebox\Guidebox();
$client->setApiKey('YOUR_API_KEY');

// Fetch all movies
$response = $client->movies->all();
$movies = $response->getResults();

// Show total results
$total_results = $response->getTotalResults();

// Fetch a single show with ID 12345
$response = $client->movies->get(12345);
$movie = $response->getResults();
```

## Testing

The Guidebox PHP Client uses PHPUnit for unit testing. In order to run the unit tests, you'll first need
to install the dependencies of the project using Composer: `php composer.phar install --dev`.
You can then run the tests using `vendor/bin/phpunit`.

Make sure you provide a `test` API Key in your `phpunit.xml`.

=======================

Copyright (c) 2017 Guidebox.com

Released under the MIT License, which can be found in the repository in `LICENSE`.
