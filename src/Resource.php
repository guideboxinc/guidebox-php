<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox;

use Exception;
use Guidebox\Exception\AuthorizationException;
use Guidebox\Exception\InternalErrorException;
use Guidebox\Exception\NetworkErrorException;
use Guidebox\Exception\RateLimitException;
use Guidebox\Exception\ResourceNotFoundException;
use Guidebox\Exception\UnexpectedErrorException;
use Guidebox\Exception\ValidationException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;

abstract class Resource implements ResourceInterface
{
    protected $guidebox;
    protected $client;

    public function __construct(Guidebox $guidebox)
    {
        $this->guidebox = $guidebox;
        $this->client   = new Client(array('base_uri' => $guidebox->getBaseUrl()));
    }

    public function all(array $query = array())
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            $this->resourceName(),
            $query
        );
    }

    public function get($id)
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            $this->resourceName() . '/' . strval($id)
        );
    }

    protected function resourceName()
    {
        $class = explode('\\', strtolower(get_called_class()));

        return array_pop($class);
    }

    protected function sendRequest($method, $version, $clientVersion, $path, array $query = array(), array $body = null)
    {
        $path    = $this->getPath($path);
        $options = $this->getOptions($version, $clientVersion, $body, $query);

        try {
            $response = $this->client->request($method, $path, $options);
            //@codeCoverageIgnoreStart
            // There is no way to induce this error intentionally.
        } catch (ConnectException $e) {
            throw new NetworkErrorException($e->getMessage());
            //@codeCoverageIgnoreEnd
        } catch (GuzzleException $e) {
            $responseErrorBody = strval($e->getResponse()->getBody());
            $errorMessage      = $this->errorMessageFromJsonBody($responseErrorBody);
            $statusCode        = $e->getResponse()->getStatusCode();

            if ($statusCode === 401) {
                throw new AuthorizationException('Unauthorized', 401);
            }

            if ($method == 'GET' && ($statusCode === 404 || $statusCode === 422)) {
                throw new ResourceNotFoundException($errorMessage, 404);
            }

            if ($method == 'POST' && $statusCode === 404) {
                throw new ResourceNotFoundException($errorMessage, 404);
            }

            if ($statusCode === 422) {
                $errors = json_decode($responseErrorBody, 1);

                $message = [];
                foreach ($errors['errors'] as $k => $v) {
                    $message[] = $v[0];
                }

                throw new ValidationException(implode(PHP_EOL, $message));
            }

            // @codeCoverageIgnoreStart
            // must induce serverside error to test this, so not testable
            if ($statusCode === 429) {
                throw new RateLimitException($errorMessage, 429);
            }

            // @codeCoverageIgnoreEnd

            // @codeCoverageIgnoreStart
            // must induce serverside error to test this, so not testable
            if ($statusCode >= 500) {
                print_r($path);
                echo "\n";
                print_r($options);
                echo "\n";
                print_r($e->getMessage());
                echo "\n";
                die();
                throw new InternalErrorException($errorMessage, $statusCode);
            }

            // @codeCoverageIgnoreEnd

            // @codeCoverageIgnoreStart
            // not possible to test this code because we don't return other status codes
            throw new UnexpectedErrorException('An Unexpected Error has occurred: ' . $e->getMessage());
        } catch (Exception $e) {
            throw new UnexpectedErrorException('An Unexpected Error has occurred: ' . $e->getMessage());
        }
        // @codeCoverageIgnoreEnd

        return new Response($response);
    }

    protected function getPath($path, array $query = array())
    {
        $path        = '/v2/' . $path;
        $queryString = '';
        if (!empty($query)) {
            $queryString = '?' . http_build_query($query);
        }
        return $path . $queryString;
    }

    protected function getOptions($version, $clientVersion, array $body = null, array $query = [])
    {
        $options = [
            'headers' => [
                'Accept'          => 'application/json; charset=utf-8',
                'User-Agent'      => 'Guidebox/v2 PhpBindings/' . $clientVersion,
                'Authorization'   => $this->guidebox->getApiKey(),
                'Guidebox-Region' => $this->guidebox->getRegion(),
            ],
            'query'   => [],
        ];

        if ($query) {
            $options['query'] = array_merge($options['query'], $query);
        }
        if ($version) {
            $options['headers']['Guidebox-Version'] = $version;
        }

        if (!$body) {
            return $options;
        }

        $body  = $this->stringifyBooleans($body);
        $files = array_filter($body, function ($element) {
            return (is_string($element) && strpos($element, '@') === 0);
        });

        if (!$files) {
            $options['form_params'] = $body;
            return $options;
        }

        $body                 = $this->flattenArray($body);
        $options['multipart'] = array();
        foreach ($body as $key => $value) {
            $element = array(
                'name'     => $key,
                'contents' => $value,
            );

            if ((is_string($value) && strpos($value, '@') === 0)) {
                $element['contents'] = fopen(substr($value, 1), 'r');
            }

            $options['multipart'][] = $element;
        }

        return $options;
    }

    /*
     * Because guzzle uses http_build_query it will turn all booleans into '' and '1' for
     * false and true respectively. This function will turn all booleans into the string
     * literal 'false' and 'true'
     */
    protected function stringifyBooleans($body)
    {
        return array_map(function ($value) {
            if (is_bool($value)) {
                return $value ? 'true' : 'false';
            } else if (is_array($value)) {
                return $this->stringifyBooleans($value);
            }
            return $value;
        }, $body);
    }

    /*
     * This method is needed because multipart guzzle requests cannot have nested data
     * This function will take:
     * array(
     *     'foo' => array(
     *         'bar' => 'baz'
     *     )
     * )
     * And convert it to:
     * array(
     *     'foo[bar]' => 'baz'
     * )
     */
    protected function flattenArray(array $body, $prefix = '')
    {
        $newBody = array();
        foreach ($body as $k => $v) {
            $key = (!strlen($prefix)) ? $k : "{$prefix}[{$k}]";
            if (is_array($v)) {
                $newBody += $this->flattenArray($v, $key);
            } else {
                $newBody[$key] = $v;
            }
        }
        return $newBody;
    }

    protected function errorMessageFromJsonBody($body)
    {
        $response = json_decode($body, true);
        if (is_array($response) && array_key_exists('error', $response)) {
            $error = $response['error'];

            return $error;
        }
        // @codeCoverageIgnoreStart
        // Pokemon handling is tough to test... "Gotta catch em all!"
        return 'An Internal Error has occurred';
        // @codeCoverageIgnoreEnd
    }
}
