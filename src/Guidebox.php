<?php

/*
 * This file is part of the Guidebox PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox;

use Guidebox\Resource\Channels;
use Guidebox\Resource\Clips;
use Guidebox\Resource\Episodes;
use Guidebox\Resource\Genres;
use Guidebox\Resource\Movies;
use Guidebox\Resource\Persons;
use Guidebox\Resource\Quota;
use Guidebox\Resource\Regions;
use Guidebox\Resource\Search;
use Guidebox\Resource\Segments;
use Guidebox\Resource\Shows;
use Guidebox\Resource\Sources;
use Guidebox\Resource\Tags;
use Guidebox\Resource\Updates;
use InvalidArgumentException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Output\ConsoleOutput;

class Guidebox extends Resource
{
    private $version;
    private $apiKey;
    private $region;
    private $clientVersion;

    const QUIET   = 16;
    const VERBOSE = 128;

    private $verbosity = self::VERBOSE;

    public function __construct($apiKey = null, $region = null, $version = null)
    {
        set_exception_handler(function ($e) {
            if (!$e instanceof Exception) {
                $e = new \ErrorException($e->getMessage(), 0, 0, $e->getFile(), $e->getLine());
            }
            (new Application)->renderException($e, $this->getOutput());
        });

        if (!is_null($apiKey)) {
            $this->setApiKey($apiKey);
        }

        $this->setRegion($region);

        $this->version       = $version;
        $this->clientVersion = '1.0.0';
        $this->guidebox      = $this;
        $this->setBaseUrl('https://api-public.guidebox.com/v2');
    }

    public function getOutput()
    {
        return new ConsoleOutput($this->verbosity);
    }

    public function setDebugMode(bool $debug)
    {
        $this->verbosity = $debug ? self::VERBOSE : self::QUIET;
        return $this;
    }

    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
        return $this;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey)
    {
        if (!is_string($apiKey) || empty($apiKey)) {
            throw new InvalidArgumentException('API Key must be a non-empty string.');
        }
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        if (!is_null($region) && !is_string($region)) {
            throw new InvalidArgumentException('Region must be a string.');
        }
        $this->region = $region ?: 'US';

        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getClientVersion()
    {
        return $this->clientVersion;
    }

    public function channels()
    {
        return new Channels($this);
    }

    public function clips()
    {
        return new Clips($this);
    }

    public function episodes()
    {
        return new Episodes($this);
    }

    public function genres()
    {
        return new Genres($this);
    }

    public function movies()
    {
        return new Movies($this);
    }

    public function persons()
    {
        return new Persons($this);
    }

    public function regions()
    {
        return new Regions($this);
    }

    public function segments()
    {
        return new Segments($this);
    }

    public function shows()
    {
        return new Shows($this);
    }

    public function sources()
    {
        return new Sources($this);
    }

    public function tags()
    {
        return new Tags($this);
    }

    public function updates()
    {
        return new Updates($this);
    }

    public function quota()
    {
        return Quota::quota(new Quota($this));
    }

    public function search()
    {
        return new Search($this);
    }

    /**
     * Convert a PHP error to an ErrorException.
     *
     * @param  int  $level
     * @param  string  $message
     * @param  string  $file
     * @param  int  $line
     * @param  array  $context
     * @return void
     *
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if ($level) {
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }
}
