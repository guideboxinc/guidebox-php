<?php

namespace Guidebox;

class Response
{

    private $response   = null;
    private $body       = null;
    private $headers    = [];
    private $statusCode = null;

    public function __construct(\GuzzleHttp\Psr7\Response $response)
    {
        $this->response   = $response;
        $this->body       = json_decode((string) $this->response->getBody(), 1);
        $this->headers    = $this->response->getHeaders();
        $this->statusCode = $this->response->getStatusCode();
    }

    public function getRawResults()
    {
        return $this->body ?: null;
    }

    /**
     * Puts the Results into an Array
     * @return array
     */
    public function getResults()
    {
        if (isset($this->body['results'])) {
            return $this->body['results'];
        }

        return $this->body ?: null;
    }

    /**
     * Returns the total number of results in the API
     * @return int
     */
    public function getTotalResults()
    {
        return isset($this->body['total_results']) ? $this->body['total_results'] : null;
    }

    /**
     * Returns the total number of results returned
     * @return int
     */
    public function getTotalReturned()
    {
        return isset($this->body['total_returned']) ? $this->body['total_returned'] : null;
    }

    /**
     * Fetches the response headers in an array
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Fetches the status code for the response
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Fetches the rate limit information
     * @return array
     */
    public function getRateLimit()
    {
        return [
            'limit'     => $this->response->getHeader('X-RateLimit-Limit'),
            'remaining' => $this->response->getHeader('X-RateLimit-Remaining'),
        ];
    }
}
