<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Resource;

use Guidebox\Resource as ResourceBase;

class Shows extends ResourceBase
{
    public function seasons($id)
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/seasons"
        );
    }

    public function images($id, $filter = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/images",
            $filter
        );
    }

    public function related($id)
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/related"
        );
    }

    public function episodes($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/episodes",
            $params
        );
    }

    public function clips($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/clips",
            $params
        );
    }

    public function segments($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/segments",
            $params
        );
    }

    public function available_content($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "shows/{$id}/available_content"
        );
    }

}
