<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Resource;

use Guidebox\Resource as ResourceBase;

class Persons extends ResourceBase
{

    public function get($id)
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "person/{$id}"
        );
    }

    public function images($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "person/{$id}/images",
            $params
        );
    }

    public function credits($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "person/{$id}/credits",
            $params
        );
    }

}
