<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Resource;

use Guidebox\Resource as ResourceBase;

class Movies extends ResourceBase
{

    public function images($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "movies/{$id}/images",
            $params
        );
    }

    public function related($id)
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "movies/{$id}/related"
        );
    }

    public function trailers($id, $params = [])
    {
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "movies/{$id}/videos",
            $params
        );
    }

}
