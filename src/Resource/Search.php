<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Resource;

use Guidebox\Resource as ResourceBase;

class Search extends ResourceBase
{
    public function all(array $options = [])
    {
        throw new UnsupportedMethodException("This method is not supported for this endpoint.");
    }

    public function get($id)
    {
        throw new UnsupportedMethodException("This method is not supported for this endpoint.");
    }

    public function movies($params = [])
    {
        $params['type'] = 'movie';
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "search",
            $params
        );
    }

    public function shows($params = [])
    {
        $params['type'] = 'show';
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "search",
            $params
        );
    }

    public function person($params = [])
    {
        $params['type'] = 'person';
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "search",
            $params
        );
    }

    public function channels($params = [])
    {
        $params['type'] = 'channel';
        return $this->sendRequest(
            'GET',
            $this->guidebox->getVersion(),
            $this->guidebox->getClientVersion(),
            "search",
            $params
        );
    }
}
