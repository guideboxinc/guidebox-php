<?php

/*
 * This file is part of the Guidebox.com PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Resource;

use Guidebox\Exception\UnsupportedMethodException;
use Guidebox\Resource as ResourceBase;

class Quota extends ResourceBase
{

    public function all(array $options = [])
    {
        throw new UnsupportedMethodException("This method is not supported for this endpoint.");
    }

    public function get($id)
    {
        throw new UnsupportedMethodException("This method is not supported for this endpoint.");
    }

    public static function quota($quota)
    {
        return $quota->sendRequest(
            'GET',
            $quota->guidebox->getVersion(),
            $quota->guidebox->getClientVersion(),
            "quota"
        );
    }
}
