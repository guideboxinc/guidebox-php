<?php

/*
 * This file is part of the Guidebox PHP Client.
 *
 * (c) 2017 Guidebox.com, https://www.guidebox.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Guidebox\Exception;

use Exception;

class UnsupportedMethodException extends Exception
{}
